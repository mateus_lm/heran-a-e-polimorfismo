#include "pentagono.hpp"

Pentagono::Pentagono (){
    set_tipo("Pentagono pentagonal");
    set_base(2.0f);
    set_altura(3.0f);//apotema
}

Pentagono::Pentagono(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

Pentagono::~Pentagono(){
    cout << "destruindo o pentagono" << endl;
}

float Pentagono::calcula_area(){
    return ((get_base() * 5 * get_altura())/2);
}
float Pentagono::calcula_perimetro(){
    return (get_base() * 5) ;
}
