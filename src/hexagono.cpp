#include "hexagono.hpp"

Hexagono::Hexagono (){
    set_tipo("Hexagono Hexagonal");
    set_base(2.0f);
    set_altura(3.0f);//apotema
}

Hexagono::Hexagono(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

Hexagono::~Hexagono(){
    cout << "destruindo o hexagono" << endl;
}

float Hexagono::calcula_area(){
    return ((get_base() * 6 * get_altura())/2);
}
float Hexagono::calcula_perimetro(){
    return (get_base() * 5) ;
}
