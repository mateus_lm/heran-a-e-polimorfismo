#include "triangulo.hpp"
#include "circulo.hpp"
#include "quadrado.hpp"
#include "paralelogra.hpp"
#include "hexagono.hpp"
#include "pentagono.hpp"

#include <string>
#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main() {

    vector<FormaGeometrica*> lista;

    lista.push_back(new FormaGeometrica());
    lista.push_back(new Quadrado());
    lista.push_back(new Triangulo());
    lista.push_back(new Circulo());
    lista.push_back(new Paralelograma());
    lista.push_back(new Hexagono());
    lista.push_back(new Pentagono());

    for (FormaGeometrica *p :lista)
    {
        cout << "Tipo: " << p->get_tipo << endl;
        cout << "Base: " << p->get_base << endl;
        cout << "Altura: " << p->get_altura << endl;
        cout << "Area: " << p->calcula_area << endl;
        cout << "Perimetro: " << p->calcula_perimetro() << endl;

    }


//  float base, altura;
//  Triangulo * t;
//  Circulo * c;
//  Quadrado * q;

//  t = new Triangulo("trianglo", 7, 2);
//  cout << "Tipo: " << t-> get_tipo() << endl;
//  cout << "Área: " << t-> calcula_area() << endl;
//  cout << "Perimetro: " << t-> calcula_perimetro() << endl;

    return 0;
}