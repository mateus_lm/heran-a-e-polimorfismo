#ifndef PARALELOGRAMA_HPP
#define PARALELOGRAMA_HPP

#include "formageometrica.hpp"

class Paralelograma : public FormaGeometrica{
public:
    Paralelograma ();
    Paralelograma (string tipo, float base, float altura);
    ~Paralelograma();
  
};

#endif 